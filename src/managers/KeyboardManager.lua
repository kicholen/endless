module(..., package.seeall)

-- windows globals
local lastKeyPressed = ""
local keysPressed = {}

-- The Key Event Listener
local function onKeyEvent( event )
        if event.phase == "down" then
            lastKeyPressed = event.keyName
            keysPressed[event.keyName] = true;
           -- table.insert(keysPressed, event.keyName)
        end
        if event.phase == "up" and event.keyName == lastKeyPressed then
            lastKeyPressed = ""
        end
        
        if event.phase == "up" then
            keysPressed[event.keyName] = nil
        end
        
	-- If the "back" key was pressed on Android, then prevent it from backing out of the app.
	-- We do this by returning true, telling the operating system that we are overriding the key.
	if (event.keyName == "back") and (platformName == "Android") then
		return true
	end

	-- Return false to indicate that this app is *not* overriding the received key.
	-- This lets the operating system execute its default handling of this key.
	return false
end

-- it returns only last pressed
function getPressedKey()
    if lastKeyPressed then
        return lastKeyPressed
    end
end

function isKeyPressed(key)
    if keysPressed[key] then
        return true
    else 
        return false
    end
end

-- Add the key callback
Runtime:addEventListener( "key", onKeyEvent );

