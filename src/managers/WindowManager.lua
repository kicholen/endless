module(..., package.seeall)

-- windows globals
_currentScene = "menuScene"
_previousScene = ""
local DEFAULT_SCREEN_FADE = "fade"
local DEFAULT_SCREEN_FADE_TIME = 500

function changeScene(sceneName, effect, timeEffect, shouldPurgeOnSceneChange)
    _previousScene = _currentScene
    _currentScene = sceneName
    if effect == nil then
        effect = DEFAULT_SCREEN_FADE
    end
    
    if effect == nil then
        timeEffect = DEFAULT_SCREEN_FADE_TIME
    end
    if shouldPurgeOnSceneChange == nil or shouldPurgeOnSceneChange == false then
        _context.storyboard.purgeOnSceneChange = false;
    else
       _context.storyboard.purgeOnSceneChange = true;
    end
    _context.storyboard.gotoScene(_currentScene, effect, timeEffect)
    return true
end

function goBackToPreviousScene()
    if _previousScene ~= _currentScene and _previousScene ~= nil then
        changeScene(_previousScene, nil, nil, true)
    end
end

function addButton(widget, buttonId, text, onReleaseFunction)
    local button = widget.newButton
    {
      id = buttonId,
      
      sheet = _context.myImageSheet,
      defaultFrame = 8,
      overFrame = 9,
      label = text,
      font = native.systemFontBold,
      fontSize = 22,
      emboss = true,
      onRelease = onReleaseFunction,
    }
    return button
end

function addButtonAndPositionCentered(widget, buttonId, text, onReleaseFunction, x, y)
    local button = addButton(widget, buttonId, text, onReleaseFunction)
    button.x = x - button.width / 2; 
    button.y = y - button.height / 2;
    return button;
end

--function addBackground()
