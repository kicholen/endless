module(..., package.seeall)

local playerSprite = require("src.sprites.PlayerSprite")
local groundSprite = require("src.sprites.GroundSprite")
local groundSpritePhysics = require("src.sprites.GroundSpritePhysics")
local ceilingSprite = require("src.sprites.CeilingSprite")
local touchQuad = require("src.sprites.TouchQuad")
local jumpOverThingySprite = require("src.sprites.JumpOverThingySprite")
local crystaliteContainer = require("src.sprites.CrystaliteContainer")
local droneSprite = require("src.sprites.drone")

function getPlayerSprite(physics, x, y)
  local sprite =  playerSprite.new()
  sprite:addToPhysics(physics)
  sprite.x = x
  sprite.y = y
  return sprite
end


function getGroundSprite()
  local groundSprite = groundSprite.new()
  return groundSprite
end

function getGroundSpritePhysics(physics, isSecondFloor)
  local groundSpritePhysics = groundSpritePhysics.new(isSecondFloor, _context.stageWidth + 100, 32)
  groundSpritePhysics:addToPhysics(physics)
  return groundSpritePhysics
end

function getCeilingSprite(physics, isSecondCeiling)
  local ceilingSprite = ceilingSprite.new(isSecondCeiling)
  ceilingSprite:addToPhysics(physics)
  return ceilingSprite
end

function getDroneSprite(physics)
  local droneSprite = droneSprite.new()
  droneSprite:addToPhysics(physics)
  return droneSprite
end

function getRightSideTouchQuad(onTouchQuadTouchCallback)
    local touchQuad = touchQuad.new(_context.centerX, _context.top, _context.stageWidth, _context.stageHeight, onTouchQuadTouchCallback)
    return touchQuad
end

function getJumpOverThingy(physics, x, y)
	local sprite = jumpOverThingySprite.new()
  sprite:addToPhysics(physics)  sprite.x = x	sprite.y = y
	return sprite
end

function getCrystaliteContainer(physics, x, y, text)
  local container = crystaliteContainer.new(x, y)
  container:addToPhysics(physics, text)
  return container
end