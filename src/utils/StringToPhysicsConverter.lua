module(..., package.seeall)

local alphabet = {}
alphabet[1] = "---#----######---#####--######--#######-#######--#####--#-----#-###-------#-#----#-#-------#-----#-#-----#-#######-######--######---#####--#######-#-----#-#-----#-#-----#-#-----#-#-----#-#######----#----#####---#####--#-------#######--#####--#######--#####---#####----###---"
alphabet[2] = "--#-#---#-----#-#-----#-#-----#-#-------#-------#-----#-#-----#--#--------#-#---#--#-------##---##-##----#-#-----#-#-----#-#-----#-#-----#----#----#-----#-#-----#-#--#--#--#---#---#---#-------#----##---#-----#-#-----#-#----#--#-------#-----#-#----#--#-----#-#-----#--#---#--"

alphabet[3] = "-#---#--#-----#-#-------#-----#-#-------#-------#-------#-----#--#--------#-#--#---#-------#-#-#-#-#-#---#-#-----#-#-----#-#-----#-#----------#----#-----#-#-----#-#--#--#---#-#-----#-#-------#----#-#---------#-------#-#----#--#-------#-----------#---#-----#-#-----#-#-----#-"

alphabet[4] = "#-----#-######--#-------#-----#-#####---#####---#--####-#######--#--------#-###----#-------#--#--#-#--#--#-#-----#-######--######---#####-----#----#-----#-#-----#-#--#--#----#-------#-------#-------#----#####---#####--#----#--######--######-----#-----#####---######-#-----#-"

alphabet[5] = "#######-#-----#-#-------#-----#-#-------#-------#-----#-#-----#--#--#-----#-#--#---#-------#-----#-#---#-#-#-----#-#-------#---#---------#----#----#-----#--#---#--#--#--#---#-#------#------#--------#---#-------------#-#######-------#-#-----#---#-----#-----#-------#-#-----#-"

alphabet[6] = "#-----#-#-----#-#-----#-#-----#-#-------#-------#-----#-#-----#--#--#-----#-#---#--#-------#-----#-#----##-#-----#-#-------#----#--#-----#----#----#-----#---#-#---#--#--#--#---#-----#-----#---------#---#-------#-----#------#--#-----#-#-----#---#-----#-----#-#-----#--#---#--"

alphabet[7] = "#-----#-######---#####--######--#######-#--------#####--#-----#-###--#####--#----#-#######-#-----#-#-----#-#######-#-------#-----#--#####-----#-----#####-----#-----##-##--#-----#----#----#######--#####-#######--#####-------#---#####---#####----#------#####---#####----###---"

alp = {}

a = {}
a[1] = "  *  " 
a[2] = " * * "   
a[3] = "*****"  
a[4] = "*   *" 
a[5] = "*   *"
table.insert(alp, a)

b = {}
b[1] = "**** " 
b[2] = "*   *"   
b[3] = "*****"  
b[4] = "*   *" 
b[5] = "*****"
table.insert(alp, b)

c = {}
c[1] = "**** " 
c[2] = "*   *"   
c[3] = "*    "  
c[4] = "*   *" 
c[5] = "**** "
table.insert(alp, c)

d = {}
d[1] = "**** " 
d[2] = "*   *"   
d[3] = "*   *"  
d[4] = "*   *" 
d[5] = "**** "
table.insert(alp, d)

e = {}
e[1] = "*****" 
e[2] = "*    "   
e[3] = "*****"  
e[4] = "*    " 
e[5] = "*****"
table.insert(alp, e)

f = {}
f[1] = "*****" 
f[2] = "*    "   
f[3] = "**** "  
f[4] = "*    " 
f[5] = "*    "
table.insert(alp, f)

g = {}
g[1] = "*****"
g[2] = "*    "   
g[3] = "* ***"  
g[4] = "*   *" 
g[5] = "*****"
table.insert(alp, g)

h = {}
h[1] = "*   *" 
h[2] = "*   *"   
h[3] = "*****"  
h[4] = "*   *" 
h[5] = "*   *"
table.insert(alp, h)

i = {}
i[1] = "*****" 
i[2] = "  *  "   
i[3] = "  *  "  
i[4] = "  *  " 
i[5] = "*****"
table.insert(alp, i)

j = {}
j[1] = "*****" 
j[2] = "  *  "   
j[3] = "  *  "  
j[4] = "* *  " 
j[5] = "***  "
table.insert(alp, j)

k = {}
k[1] = "*   *" 
k[2] = "*  * "   
k[3] = "* *  "  
k[4] = "*  * " 
k[5] = "*   *"
table.insert(alp, k)

l = {}
l[1] = "*    " 
l[2] = "*    "   
l[3] = "*    "  
l[4] = "*    " 
l[5] = "*****"
table.insert(alp, l)

m = {}
m[1] = "*   *" 
m[2] = "** **"   
m[3] = "* * *"  
m[4] = "*   *" 
m[5] = "*   *"
table.insert(alp, m)

n = {}
n[1] = "*   *" 
n[2] = "*  **"   
n[3] = "* * *"  
n[4] = "**  *" 
n[5] = "*   *"
table.insert(alp, n)

o = {}
o[1] = " *** " 
o[2] = "*   *"   
o[3] = "*   *"  
o[4] = "*   *" 
o[5] = " *** "
table.insert(alp, o)

p = {}
p[1] = "**** " 
p[2] = "*   *"   
p[3] = "*****"  
p[4] = "*    " 
p[5] = "*    "
table.insert(alp, b)

q = {}
q[1] = "*****" 
q[2] = "*   *"   
q[3] = "* * *"  
q[4] = "*  **" 
q[5] = "*****"
table.insert(alp, q)

r = {}
r[1] = "**** " 
r[2] = "*   *"   
r[3] = "*****"  
r[4] = "*   *" 
r[5] = "*   *"
table.insert(alp, r)

s = {}
s[1] = "**** " 
s[2] = "*    "   
s[3] = "*****"  
s[4] = "    *" 
s[5] = " ****"
table.insert(alp, s)

t = {}
t[1] = "*****" 
t[2] = "  *  "   
t[3] = "  *  "  
t[4] = "  *  " 
t[5] = "  *  "
table.insert(alp, t)

u = {}
u[1] = "*   *" 
u[2] = "*   *"   
u[3] = "*   *"  
u[4] = "*   *" 
u[5] = " *** "
table.insert(alp, u)

v = {}
v[1] = "*   *" 
v[2] = "*   *"   
v[3] = "*   *"  
v[4] = " * * " 
v[5] = "  *  "
table.insert(alp, v)

w = {}
w[1] = "*   *" 
w[2] = "*   *"   
w[3] = "* * *"   
w[4] = "** **" 
w[5] = "*   *"
table.insert(alp, w)

x = {}
x[1] = "*   *" 
x[2] = " * * "   
x[3] = "  *  "  
x[4] = " * * " 
x[5] = "*   *"
table.insert(alp, x)

y = {}
y[1] = "*   *" 
y[2] = " * * "   
y[3] = "  *  "  
y[4] = "  *  " 
y[5] = "  *  "
table.insert(alp, y)

z = {}
z[1] = "*****" 
z[2] = "   * "   
z[3] = "  *  "  
z[4] = " *   " 
z[5] = "*****"
table.insert(alp, z)

n1 = {}
n1[1] = "  ** " 
n1[2] = " * * "   
n1[3] = "*  * "  
n1[4] = "   * " 
n1[5] = "*****"
table.insert(alp, n1)

n2 = {}
n2[1] = " *** " 
n2[2] = "*   *"   
n2[3] = "   * "  
n2[4] = "  *  " 
n2[5] = "*****"
table.insert(alp, n2)

n3 = {}
n3[1] = " ****" 
n3[2] = "*   *"   
n3[3] = "  ***"  
n3[4] = "*   *" 
n3[5] = " *** "
table.insert(alp, n3)

n4 = {}
n4[1] = "*   *" 
n4[2] = "*   *"   
n4[3] = "*****"  
n4[4] = "    *" 
n4[5] = "    *"
table.insert(alp, n4)

n5 = {}
n5[1] = "*****" 
n5[2] = "*    "   
n5[3] = "*****"  
n5[4] = "    *" 
n5[5] = "*****"
table.insert(alp, n5)

n6 = {}
n6[1] = "*****" 
n6[2] = "*    "   
n6[3] = "*****"  
n6[4] = "*   *" 
n6[5] = "*****"
table.insert(alp, n6)

n7 = {}
n7[1] = "*****" 
n7[2] = "    *"   
n7[3] = "   * "  
n7[4] = "  *  " 
n7[5] = " *   "
table.insert(alp, n7)

n8 = {}
n8[1] = "*****" 
n8[2] = "*   *"   
n8[3] = "*****"  
n8[4] = "*   *" 
n8[5] = "*****"
table.insert(alp, n8)

n9 = {}
n9[1] = "*****" 
n9[2] = "*   *"   
n9[3] = "*****"  
n9[4] = "    *" 
n9[5] = "    *"
table.insert(alp, n9)

n0 = {}
n0[1] = "*****" 
n0[2] = "*   *"   
n0[3] = "*   *"  
n0[4] = "*   *" 
n0[5] = "*****"
table.insert(alp, n1)



-- for debug purposes
local string = {}
local aString = {}
aString[1] = ""
aString[2] = ""
aString[3] = ""
aString[4] = ""
aString[5] = ""

-- 
local crystaliteFileName = "statics/resources/11.png"
local itemWidth = 16
local itemHeight = 16

local function insert(ss)

  if ss == "a" or ss == "A" then
    for i,v in ipairs(a) do
      aString[i] = aString[i] .. " " .. v
    end
  end
  if ss == "b" or ss == "B" then
      for i,v in ipairs(b) do
          aString[i] = aString[i] .. " " .. v
      end
  end
  if ss == "c" or ss == "C" then
      for i,v in ipairs(c) do
          aString[i] = aString[i] .. " " .. v
      end
  end
  if ss == "d" or ss == "D" then
      for i,v in ipairs(d) do
          aString[i] = aString[i] .. " " .. v
      end
  end
  if ss == "e" or ss == "E" then
      for i,v in ipairs(e) do
          aString[i] = aString[i] .. " " .. v
      end
  end
  if ss == "f" or ss == "F" then
      for i,v in ipairs(f) do
          aString[i] = aString[i] .. " " .. v
      end
  end
  if ss == "g" or ss == "G" then
      for i,v in ipairs(g) do
          aString[i] = aString[i] .. " " .. v
      end
  end
  if ss == "h" or ss == "H" then
      for i,v in ipairs(h) do
          aString[i] = aString[i] .. " " .. v
      end
  end
  if ss == "i" or ss == "I" then
      for i,v in ipairs(i) do
          aString[i] = aString[i] .. " " .. v
      end
  end
  if ss == "j" or ss == "J" then
      for i,v in ipairs(j) do
          aString[i] = aString[i] .. " " .. v
      end
  end
  if ss == "k" or ss == "K" then
      for i,v in ipairs(k) do
          aString[i] = aString[i] .. " " .. v
      end
  end
  if ss == "l" or ss == "L" then
      for i,v in ipairs(l) do
          aString[i] = aString[i] .. " " .. v
      end
  end
  if ss == "m" or ss == "M" then
      for i,v in ipairs(m) do
          aString[i] = aString[i] .. " " .. v
      end
  end
  if ss == "n" or ss == "N" then
      for i,v in ipairs(n) do
          aString[i] = aString[i] .. " " .. v
      end
  end
  if ss == "o" or ss == "O" then
      for i,v in ipairs(o) do
          aString[i] = aString[i] .. " " .. v
      end
  end
  if ss == "p" or ss == "P" then
      for i,v in ipairs(p) do
          aString[i] = aString[i] .. " " .. v
      end
  end
  if ss == "q" or ss == "Q" then
      for i,v in ipairs(q) do
          aString[i] = aString[i] .. " " .. v
      end
  end
  if ss == "r" or ss == "R" then
      for i,v in ipairs(r) do
          aString[i] = aString[i] .. " " .. v
      end
  end
  if ss == "s" or ss == "S" then
      for i,v in ipairs(s) do
          aString[i] = aString[i] .. " " .. v
      end
  end
  if ss == "t" or ss == "T" then
      for i,v in ipairs(t) do
          aString[i] = aString[i] .. " " .. v
      end
  end
  if ss == "u" or ss == "U" then
      for i,v in ipairs(u) do
          aString[i] = aString[i] .. " " .. v
      end
  end
  if ss == "v" or ss == "V" then
      for i,v in ipairs(v) do
          aString[i] = aString[i] .. " " .. v
      end
  end
  if ss == "w" or ss == "W" then
      for i,v in ipairs(w) do
          aString[i] = aString[i] .. " " .. v
      end
  end
  if ss == "x" or ss == "X" then
      for i,v in ipairs(x) do
          aString[i] = aString[i] .. " " .. v
      end
  end
  if ss == "y" or ss == "Y" then
      for i,v in ipairs(y) do
          aString[i] = aString[i] .. " " .. v
      end
  end
  if ss == "z" or ss == "Z" then
      for i,v in ipairs(z) do
          aString[i] = aString[i] .. " " .. v
      end
  end
  if ss == "0" then
      for i,v in ipairs(n0) do
          aString[i] = aString[i] .. " " .. v
      end
  end
  if ss == "1" then
      for i,v in ipairs(n1) do
          aString[i] = aString[i] .. " " .. v
      end
  end
  if ss == "2" then
      for i,v in ipairs(n2) do
          aString[i] = aString[i] .. " " .. v
      end
  end
  if ss == "3" then
      for i,v in ipairs(n3) do
          aString[i] = aString[i] .. " " .. v
      end
  end
  if ss == "4" then
      for i,v in ipairs(n4) do
          aString[i] = aString[i] .. " " .. v
      end
  end
  if ss == "5" then
      for i,v in ipairs(n5) do
          aString[i] = aString[i] .. " " .. v
      end
  end
  if ss == "6" then
      for i,v in ipairs(n6) do
          aString[i] = aString[i] .. " " .. v
      end
  end
  if ss == "7" then
      for i,v in ipairs(n7) do
          aString[i] = aString[i] .. " " .. v
      end
  end
  if ss == "8" then
      for i,v in ipairs(n8) do
          aString[i] = aString[i] .. " " .. v
      end
  end
  if ss == "9" then
      for i,v in ipairs(n9) do
          aString[i] = aString[i] .. " " .. v
      end
  end
  if ss == " " then
      for i=1,5 do
          aString[i] = aString[i] .. " "
      end
  end
end

function getObjects(str)
  local offset = 5
  local children = {}
  local positionX = 0
  local positionY = 0
  local isFirstLoop = true
  
  string = {}
  aString = {}
  aString[1] = ""
  aString[2] = ""
  aString[3] = ""
  aString[4] = ""
  aString[5] = ""
  
  for i=1, #str do
    table.insert(string, str:sub(i,i))
  end

  for i,v in ipairs(string) do
    insert(v)
  end
  
  for i,v in ipairs(aString) do
    positionX = 0
    if isFirstLoop == false then
      positionY = positionY + itemHeight + offset
    end
    isFirstLoop = false
    for i=1, #v do
      positionX = positionX + itemWidth + offset
      if v:sub(i,i) == "*" then
        local displayObject = display.newImageRect( crystaliteFileName, itemWidth, itemHeight )
        displayObject.x = positionX
        displayObject.y = positionY
        table.insert(children, displayObject)
      end
    end
  end
  
  return children
end
