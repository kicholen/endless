----------------------------------------------------------------------------------
--
-- scenetemplate.lua
--
----------------------------------------------------------------------------------
local scene = _context.storyboard.newScene()
local widget = require( "widget" )

-- Called when the scene's view does not exist:
function scene:createScene( event )
	local group = self.view
  _context.playhaven.contentRequest("main_menu", true)
  
	local background = display.newImageRect(_context.myImageSheet, _context.sheetInfo:getFrameIndex("bg2"),  _context.backgroundWidth, _context.backgroundHeight)
        background.x = _context.left
        background.y = _context.top
	group:insert( background )
	-----------------------------------------------------------------------------
		
	--	CREATE display objects and add them to 'group' here.
	--	Example use-case: Restore 'group' from previously saved state.
	
	-----------------------------------------------------------------------------
        local playButtonHandler = function( event )
            _context.windowManager.changeScene("src.scenes.gameScene", nil, nil, true)
        end 
  
         --storyboard.purgeScene( "gameScene" )
        group:insert(_context.windowManager.addButtonAndPositionCentered(widget, "playButton", "PLAY", playButtonHandler, _context.centerX, _context.centerY))
end


-- Called immediately after scene has moved onscreen:
function scene:enterScene( event )
	local group = self.view
	
 
	-----------------------------------------------------------------------------
		
	--	INSERT code here (e.g. start timers, load audio, start listeners, etc.)
	
	-----------------------------------------------------------------------------
	
end


-- Called when scene is about to move offscreen:
function scene:exitScene( event )
	local group = self.view
	
	-----------------------------------------------------------------------------
	
	--	INSERT code here (e.g. stop timers, remove listeners, unload sounds, etc.)
	
	-----------------------------------------------------------------------------
	
end


-- Called prior to the removal of scene's "view" (display group)
function scene:destroyScene( event )
	local group = self.view
	
  
	-----------------------------------------------------------------------------
	
	--	INSERT code here (e.g. remove listeners, widgets, save state, etc.)
	
	-----------------------------------------------------------------------------
	
end

---------------------------------------------------------------------------------
-- END OF YOUR IMPLEMENTATION
---------------------------------------------------------------------------------

-- "createScene" event is dispatched if scene's view does not exist
scene:addEventListener( "createScene", scene )

-- "enterScene" event is dispatched whenever scene transition has finished
scene:addEventListener( "enterScene", scene )

-- "exitScene" event is dispatched before next scene's transition begins
scene:addEventListener( "exitScene", scene )

-- "destroyScene" event is dispatched before view is unloaded, which can be
-- automatically unloaded in low memory situations, or explicitly via a call to
-- storyboard.purgeScene() or storyboard.removeScene().
scene:addEventListener( "destroyScene", scene )

---------------------------------------------------------------------------------

return scene