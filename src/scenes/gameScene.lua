----------------------------------------------------------------------------------
--
-- scenetemplate.lua
--
----------------------------------------------------------------------------------
local scene = _context.storyboard.newScene()
local physics = require "physics"
local widget = require "widget"


local displayMain
local displayPhysics
local displayGui
local displayBackground

local playerSprite

local previousDeltaTime
-- every object that is in this loop depends on player current position
local tickersOnPlayer = {}
-- normal objects
local tickers = {}

function addToLoop(object, isRelatedWithPlayerPosition)
	assert(object ~= nil, "You can't pass nil values to game loop")
	if isRelatedWithPlayerPosition == true then
		local index = table.indexOf(tickersOnPlayer, object)
		if (index == nil) then
			return table.insert(tickersOnPlayer, object)
		else
			print(object .. " is already in the loop.")
		end
	else 
		local index = table.indexOf(tickers, object)
		if (index == nil) then
			return table.insert(tickers, object)
		else
			print(object .. " is already in the loop.")
		end
	end

end

function removeFromLoop(object)
	print(object .. " is being removed from game loop.")
	for i,v in ipairs(tickersOnPlayer) do
		if (v == object) then
			table.remove(tickersOnPlayer, i)
			return true
		end	
	end
	
	for i,v in ipairs(tickers) do
		if (v == object) then
			table.remove(tickers, i)
			return true
		end	
	end
	
	print(object .. " was not found in game loop.")
	return false
end

function removeAllFromLoop()	for k,v in pairs(tickers) do 		tickers[k]=nil 	end	for k,v in pairs(tickersOnPlayer) do 		tickersOnPlayer[k]=nil 	endend
function setupLayers()
  displayMain = display.newGroup()
  displayPhysics = display.newGroup()
  displayGui = display.newGroup()
  displayBackground = display.newGroup()
  
  displayMain:insert(displayBackground)
  displayMain:insert(displayPhysics)
  displayMain:insert(displayGui)
  
  displayPhysics:toFront()
  displayGui:toFront()
end

-- Called when the scene's view does not exist:
function scene:createScene( event )
  setupLayers()
  local offset = 5
  physics.start(); physics.pause()
  physics.setGravity( 0, 19.8 )
        
	--local backgroundImg = display.newImageRect(_context.myImageSheet, _context.sheetInfo:getFrameIndex("tlov1"),  _context.backgroundWidth, _context.backgroundHeight)
  local backgroundImg = display.newImage("statics/resources/493-1.png", _context.backgroundWidth, _context.backgroundHeigh)
  backgroundImg.x = _context.left
  backgroundImg.y = _context.top
  
  local backButtonHandler = function (event)
    _context.windowManager.goBackToPreviousScene()
  end
        -- here is display.screenOriginX, need some fucntion to position display object properly no matter of resolution, built in screen offset
  local backButton = _context.windowManager.addButton(widget, "backButton", "<-", backButtonHandler)
  backButton.x = _context.stageWidth - backButton.width - offset
  backButton.y = _context.stageHeight - backButton.height - offset
  
  playerSprite = _context.spritesManager.getPlayerSprite(physics, _context.left + 40, _context.top + 40)
  playerSprite:play()
  addToLoop(playerSprite)
  local stupidLua = function (touchPhase)
    playerSprite:setTouchQuadPhase(touchPhase)
  end
  rightTouchQuad = _context.spritesManager.getRightSideTouchQuad(stupidLua)
  local stupidFuckLua = function ()
      return playerSprite:getTouchHoldCount()
  end

  _context.addDebugText("touchHoldCounter: ", stupidFuckLua)
  
  local stupidItIs = function ()
    if playerSprite ~= nil then
      local x, y = playerSprite:getVelocity()
      return x
    end
    return 0
  end
  
  _context.addDebugText("linearVelocity: ", stupidItIs)
  -- add scrolling ground and ceiling
	local groundSprite = _context.spritesManager.getGroundSprite()
	addToLoop(groundSprite, true)
	local groundSpritePhysics1 = _context.spritesManager.getGroundSpritePhysics(physics, false)
	addToLoop(groundSpritePhysics1, true)
	local groundSpritePhysics2 = _context.spritesManager.getGroundSpritePhysics(physics, true)
	addToLoop(groundSpritePhysics2, true)

  local fullCeiling1 =  _context.spritesManager.getCeilingSprite(physics, false)
  
 
  addToLoop(fullCeiling1, true)

  
  local drone = _context.spritesManager.getDroneSprite(physics)
  local tryToJumpOverIt = _context.spritesManager.getJumpOverThingy(physics, _context.stageWidth,  _context.bottom - 300 )
  local tryToJumpOverIt2 = _context.spritesManager.getJumpOverThingy(physics, _context.stageWidth + _context.stageWidth / 2,  _context.bottom - 100 )  local tryToJumpOverIt3 = _context.spritesManager.getJumpOverThingy(physics, _context.stageWidth + _context.stageWidth / 2 + tryToJumpOverIt.width,  _context.bottom - 100 )  local tryToJumpOverIt4 = _context.spritesManager.getJumpOverThingy(physics, _context.stageWidth + _context.stageWidth / 2 + 2 * tryToJumpOverIt.width,  _context.bottom - 100 )	local tryToJumpOverIt5 = _context.spritesManager.getJumpOverThingy(physics, _context.stageWidth + _context.stageWidth / 2 + 3 * tryToJumpOverIt.width,  _context.bottom - 100 )
  local tree = display.newImage("statics/graphics/tree.png", _context.centerX, _context.centerY)
  -- addition order is important !
  
  local crystalContainer = _context.spritesManager.getCrystaliteContainer(physics, 3 * _context.stageWidth, _context.top + 100, "DUPAA 1111")
  local crystalContainerChildren = crystalContainer:getChildren()
  addToLoop(crystalContainer, true)
  
  displayBackground:insert(backgroundImg)
  for k,v in pairs(crystalContainerChildren) do
    displayPhysics:insert(crystalContainerChildren[k])
  end
	displayPhysics:insert(tryToJumpOverIt)	displayPhysics:insert(tryToJumpOverIt2)	displayPhysics:insert(tryToJumpOverIt3)	displayPhysics:insert(tryToJumpOverIt4)	displayPhysics:insert(tryToJumpOverIt5)
  displayPhysics:insert(drone)
    
  for i in ipairs(fullCeiling1) do
    displayPhysics:insert(groundSprite[i])
    displayPhysics:insert(fullCeiling1[i])
  end
  
  displayPhysics:insert(groundSpritePhysics1)
  displayPhysics:insert(groundSpritePhysics2)
  
	--displayPhysics:insert(ceilingSprite2)
  displayPhysics:insert(tree)
  displayPhysics:insert(playerSprite)
  displayGui:insert(rightTouchQuad)
  displayGui:insert(backButton)
end

local function onFrameChanged(event)
  local deltaTime = event.time - previousDeltaTime
  previousDeltaTime = event.time
    
	for i,object in ipairs(tickers) do
		object:onFrameChanged(deltaTime)
	end
	
	local xPosition = playerSprite.x 
  local yPosition = playerSprite.y
	for i,object in ipairs(tickersOnPlayer) do
		object:onFrameChanged(xPosition, yPosition)
	end
	
	displayPhysics.x = - xPosition
end

-- Called immediately after scene has moved onscreen:
function scene:enterScene( event )
  physics.start()
  physics.setDrawMode("hybrid")
  previousDeltaTime = system.getTimer()
  Runtime:addEventListener("enterFrame", onFrameChanged)
  
  -- colision handler for player
  local stupidFuckLua = function (e)
      playerSprite:collisionHandler(e)
  end
  if playerSprite ~= nil then
      playerSprite:addEventListener("collision", stupidFuckLua)
  end
	-----------------------------------------------------------------------------
		
	--	INSERT code here (e.g. start timers, load audio, start listeners, etc.)
	
	-----------------------------------------------------------------------------
	
end


-- Called when scene is about to move offscreen:
function scene:exitScene( event )
  Runtime:removeEventListener("enterFrame", onFrameChanged)
	-----------------------------------------------------------------------------
	
	--	INSERT code here (e.g. stop timers, remove listeners, unload sounds, etc.)
	
	-----------------------------------------------------------------------------
	
end


-- Called prior to the removal of scene's "view" (display group)
function scene:destroyScene( event )
  for i = displayMain.numChildren, 1, -1 do
      for j = displayMain[i].numChildren, 1, -1 do
          displayMain[i][j].parent:remove(displayMain[i][j])
      end
  end
      removeAllFromLoop()    
	displayMain:removeSelf()
	physics.stop()
	_context.removeDebugTexts()
	-----------------------------------------------------------------------------
	
	--	INSERT code here (e.g. remove listeners, widgets, save state, etc.)
	
	-----------------------------------------------------------------------------
	
end

---------------------------------------------------------------------------------
-- END OF YOUR IMPLEMENTATION
---------------------------------------------------------------------------------

-- "createScene" event is dispatched if scene's view does not exist
scene:addEventListener( "createScene", scene )

-- "enterScene" event is dispatched whenever scene transition has finished
scene:addEventListener( "enterScene", scene )

-- "exitScene" event is dispatched before next scene's transition begins
scene:addEventListener( "exitScene", scene )

-- "destroyScene" event is dispatched before view is unloaded, which can be
-- automatically unloaded in low memory situations, or explicitly via a call to
-- storyboard.purgeScene() or storyboard.removeScene().
scene:addEventListener( "destroyScene", scene )

---------------------------------------------------------------------------------

return scene