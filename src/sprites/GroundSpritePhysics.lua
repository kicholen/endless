module(..., package.seeall)

function new(isSecondFloor, width, height)
  local groundSprite = display.newRect(0, 0, width, height)
  groundSprite.isVisible = false
  local _isSecondFloor = isSecondFloor
  
  function groundSprite:addToPhysics(physics)
    physics.addBody(groundSprite, "static", { friction = 0.5, bounce = 0.0 })
    
    if _isSecondFloor == nil or _isSecondFloor == false then
        self.x = _context.left
    else
        self.x = _context.left + self.width
    end
    self.y = _context.bottom - self.height
  end
    
    
  function groundSprite:dispose()
      self:removeSelf()
      self = nil
  end
  
  function groundSprite:onFrameChanged(playerXPosition, playerYPosition)
    if _isSecondFloor == false then
      if self.x + self.width + _context.left + 80< playerXPosition  then
          self:translate( 2 * self.width, 0)
      end
    else 
    if self.x + self.width + _context.left  + 80 < playerXPosition  then
      self:translate(2 * self.width,  0)
    end
      end
  end
    
  return groundSprite
end