module(..., package.seeall)
local spriteSheet = require("statics.pixel.drone1") -- lua file that Texture packer published 
require "Content"

function new()
  
  local droneSpriteSheet = graphics.newImageSheet( "statics/pixel/drone1.png", spriteSheet:getSheet())
    local spriteOptions ={ name="drone", frames = {1, 2, 3, 4 }, time=400 }

    local droneSprite = display.newSprite( droneSpriteSheet, spriteOptions )
    
    
    droneSprite.x = 4000
    droneSprite.y = 200
    droneSprite:play()
  function droneSprite:addToPhysics(physics)
    physics.addBody(droneSprite, "static", {friction = 0.5, bounce = 0.0})
  end
    return droneSprite
  end