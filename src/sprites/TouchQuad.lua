module(..., package.seeall)

function new(_x, _y, _width, _height, onTouchActionCallback)
    local _onTouchActionCallback = onTouchActionCallback
    
    local touchQuad = display.newRect(_x, _y, _width, _height) --newImage("statics/landtest/floor1.png", defaultWidth, defaultHeight)
    touchQuad:setFillColor(0.5, 0.5, 0.5, 0.2)
    
    local function onTouchAction(event)
      _onTouchActionCallback(event.phase)
    end
    
    function touchQuad:dispose()
      self:removeEventListener("touch", onTouchAction)
      self:removeSelf()
      self = nil
    end
    
    touchQuad:addEventListener("touch", onTouchAction)
    return touchQuad
end