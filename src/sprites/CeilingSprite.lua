module(..., package.seeall)

function new(isSecondCeiling)

	local fullCeiling = {}
  local loopIter = 0
  
  repeat
    loopIter = loopIter + 1
    fullCeiling[loopIter] = display.newImage("statics/pixel/box.png")
    fullCeiling[loopIter].x = fullCeiling[loopIter].width * loopIter - 2 * fullCeiling[loopIter].width
    fullCeiling[loopIter].y = 0
  until loopIter * fullCeiling[loopIter].width >= 2 * display.contentWidth
    
    
  function fullCeiling:addToPhysics(physics)
    
    for i in ipairs(fullCeiling) do
      physics.addBody(fullCeiling[i], "static", {friction = 0.5, bounce = 0.0})
    end
  end
    
    
  function fullCeiling:dispose()
    self:removeSelf()
    self = nil
  end
  
  function fullCeiling:onFrameChanged(playerXPosition, playerYPosition)
    
    for i in ipairs(fullCeiling) do
      if fullCeiling[i].x + fullCeiling[i].width + _context.left + 100< playerXPosition  then
        fullCeiling[i]:translate( 2 * display.contentWidth, 0)
      end
    end
  end
  
  return fullCeiling
end