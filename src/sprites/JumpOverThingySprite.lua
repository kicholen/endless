module(..., package.seeall)

function new()

  local boxThingySprite = display.newImage("statics/graphics/306.png")
  
  function boxThingySprite:addToPhysics(physics)
    physics.addBody(boxThingySprite, "static", { friction = 0.5, bounce = 0.0 })
        
    self.x = _context.stageWidth / 2
    self.y = _context.bottom - self.height 
  end
    
    
  function boxThingySprite:dispose()
      self:removeSelf()
      self = nil
  end
  
  function boxThingySprite:onFrameChanged(playerXPosition, playerYPosition)
  end
  
  return boxThingySprite
end