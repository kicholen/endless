module(..., package.seeall)

function new(isSecondFloor)

--[[
    local groundSprite = display.newImage("statics/landtest/floor1.png")--display.newImageRect(_context.myImageSheet, _context.sheetInfo:getFrameIndex("trawa"),  defaultWidth, defaultHeight)
	local _isSecondFloor = isSecondFloor	]]--
  
  local fullGround = {}
  local loopIter = 0
  
  repeat
    loopIter = loopIter + 1
    fullGround[loopIter] = display.newImage("statics/pixel/box.png")
    fullGround[loopIter].x = fullGround[loopIter].width * loopIter - 2 * fullGround[loopIter].width
    fullGround[loopIter].y = _context.bottom - fullGround[loopIter].height
  until loopIter * fullGround[loopIter].width >= 2 * display.contentWidth
    
    
  function fullGround:addToPhysics(physics)
    
    --for i in ipairs(fullGround) do
    --  physics.addBody(fullGround[i], "static", {friction = 0.5, bounce = 0.0})
    --end
  end
    
    
  function fullGround:dispose()
    self:removeSelf()
    self = nil
  end
  
  function fullGround:onFrameChanged(playerXPosition, playerYPosition)
    
    for i in ipairs(fullGround) do
      if fullGround[i].x + fullGround[i].width + _context.left + 100< playerXPosition  then
        fullGround[i]:translate( 2 * display.contentWidth, 0)
      end
    end
  end
  

    return fullGround
end

  --[[
    function groundSprite:addToPhysics(physics)
      physics.addBody(groundSprite, "static", { friction = 0.5, bounce = 0.0, shape = groundShape })
      
      if _isSecondFloor == nil or _isSecondFloor == false then
          self.x = _context.left
      else
          self.x = _context.left + self.width
      end
      self.y = _context.bottom - self.height / 3
    end
    
    
    function groundSprite:dispose()
        self:removeSelf()
        self = nil
    end
    
    function groundSprite:onFrameChanged(playerXPosition, playerYPosition)
    	if _isSecondFloor == false then
        if self.x + self.width + _context.left + 80< playerXPosition  then
            self:translate( 2 * self.width, 0)
        end
    	else 
			if self.x + self.width + _context.left  + 80 < playerXPosition  then
				self:translate(2 * self.width,  0)
			end
        end
    end
    ]]--
--function getGroundsToFillScreen(physics)
--   if (allGroundSprites) then
--        for k,v in pairs(allGroundSprites) do allGroundSprites[k]=nil end
--    end
--    lastSpriteXPosition = _context.left
--    allGroundSprites[#allGroundSprites + 1] = getGround(physics)
--    local howManyGroundSpriteNeed = _context.stageWidth / allGroundSprites[#allGroundSprites].width;
--
--    for i = 1, howManyGroundSpriteNeed do
--        allGroundSprites[#allGroundSprites + 1] = getGround(physics)
--    end
--
--    return allGroundSprites
--end