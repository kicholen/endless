module(..., package.seeall)
local spriteSheet = require("statics.player1") -- lua file that Texture packer published 
require "Content"
function new()
    local playerSpriteSheet = graphics.newImageSheet( "statics/player1.png", spriteSheet:getSheet())
    local spriteOptions = { 
        { name="run", frames = {1, 2, 3, 2, 4, 5, 6 }, time=600, loopDirection= "bounce"},
        { name="jump", frames = {7, 8 }, time=400},
        { name="fall", frames = {1 }, time=600}
      }
    local playerSprite = display.newSprite( playerSpriteSheet, spriteOptions )
    -- for jumping
    local mass = 10
    local touchHold = 0
    local force = 0
    local numFootContacts = 0
    local jumpTimeout = 0
    local canJump = false
    local minJump = false
    local spriteSeqenceStatus = 1
    -- sensors
    local _touchQuadPhase = "ended"
    local sensorHeight = 2
    
    function playerSprite:addToPhysics(physics)
      physics.addBody(self, "dynamic",
        { density = 1.0, friction = 0.3, bounce = 0.0 }, 
        { shape = { - self.width / 2 + self.width / 20, self.height / 2 - sensorHeight / 2,
                    - self.width / 2 + self.width / 20, self.height / 2 + sensorHeight / 2,
                    self.width / 2 - self.width / 20, self.height / 2 + sensorHeight / 2,
                    self.width / 2 - self.width / 20, self.height / 2 - sensorHeight / 2
                  },
          isSensor = true 
        },
        { 
          radius = self.height / 2 * 3,
          isSensor = true
        }
      )
      self.isFixedRotation = true
      self.linearDamping = 0.4
      self.angularDamping = 0.6
        
      --self.collision = collisionHandler
    end
    
    function playerSprite:spriteSeq(nam)
        if self.sequence ~= nam then
          self:setSequence(nam)
          self:play()
        end
        
    end
    function playerSprite:play()
      self:play()
    end
    
    function playerSprite:onFrameChanged(deltaTime)
      -- player velocity      local velocityX, velocityY = self:getLinearVelocity()
      velocityX = content.PLAYER_DEFAULT_VELOCITY
      self:setLinearVelocity(velocityX, velocityY);
      -- jump
      if jumpTimeout > -1 then
        jumpTimeout = jumpTimeout - 1
      end
      if _touchQuadPhase == "ended" then
        jumpTimeout = - 1
        touchHold = 0;
        canJump = false;
        if numFootContacts > 0 then
          self:spriteSeq( "run" )
        else
          self:spriteSeq("fall")
        end
      --elseif _touchQuadPhase == "began" then

      else
        self:spriteSeq( "jump" )
        self:jump()
      end
      
    end
    
    function playerSprite:collisionHandler(event)
      -- ground touch sensor
      if event.selfElement == 2 then -- and event.other.objType == "ground" 
        if event.phase == "began" then
          numFootContacts = numFootContacts + 1
       elseif event.phase == "ended" then
          numFootContacts = numFootContacts - 1
        end
      elseif event.selfElement == 3 and event.other.objectType == content.CRYSTALITE_TYPE then
        if event.phase == "began" then
          event.other.wasTouched = true
        end
      end
    end
    
    function playerSprite:setTouchQuadPhase(touchQuadPhase)
      _touchQuadPhase = touchQuadPhase
    end
    
    function playerSprite:jump()
      touchHold = touchHold + 1
      if numFootContacts >= 1 and jumpTimeout <= 0 then
        canJump = true
        jumpTimeout = 10
        minJump = true
      end
      
      if true then
        -- when it will go beyond 20 frames
        if touchHold > 20 then
          touchHold = 15
          canJump = false
        end
        
        if minJump then
          force = 10
          self:applyLinearImpulse(0, -force, self.x + self.width / 2, self.y - self.height / 2)
          minJump = false
        else
          force = 5 * 10
          self:applyForce(0, -force, self.x + self.width / 2, self.y - self.height / 2)
        end
      end
    end
    
    function playerSprite:getVelocity()
      if self ~= nil then
        return self:getLinearVelocity()
      else 
        return 0
      end
    end
    
    function playerSprite:getXPosition()
      return self.x
    end
    
    function playerSprite:getTouchHoldCount()
      return touchHold
    end
    
    function playerSprite:dispose()
      self:removeSelf()
      self = nil
    end
    
    return playerSprite
end