module(..., package.seeall)
require "Content"
local converter = require "src.utils.StringToPhysicsConverter"
local tween = require "src.utils.Tween"

function new(derivedX, derivedY)
  local crystaliteFileName = "statics/resources/11.png"
  local crystalContainer = {}
  --[[
  local dispObj_1 = display.newImageRect( crystaliteFileName, 22, 19 )
  dispObj_1.x = 314
  dispObj_1.y = 195
  --crystalContainer:insert(dispObj_1)

  local dispObj_2 = display.newImageRect( crystaliteFileName, 22, 19 )
  dispObj_2.x = 282
  dispObj_2.y = 199
  --crystalContainer:insert(dispObj_2)

  local dispObj_3 = display.newImageRect( crystaliteFileName, 22, 19 )
  dispObj_3.x = 206
  dispObj_3.y = 195
  --crystalContainer:insert(dispObj_3)
  
  local dispObj_4 = display.newImageRect( crystaliteFileName, 22, 19 )
  dispObj_4.x = 206
  dispObj_4.y = 50
  --crystalContainer:insert(dispObj_4)
  
  local dispObj_5 = display.newImageRect( crystaliteFileName, 22, 19 )
  dispObj_5.x = 243
  dispObj_5.y = 49
  --crystalContainer:insert(dispObj_5)
  
  local dispObj_6 = display.newImageRect( crystaliteFileName, 22, 19 )
  dispObj_6.x = 245
  dispObj_6.y = 138
  --crystalContainer:insert(dispObj_6)
  
  local dispObj_7 = display.newImageRect( crystaliteFileName, 22, 19 )
  dispObj_7.x = 208
  dispObj_7.y = 170
  --crystalContainer:insert(dispObj_7)
  
  local dispObj_8 = display.newImageRect( crystaliteFileName, 22, 19 )
  dispObj_8.x = 245
  dispObj_8.y = 197
  --crystalContainer:insert(dispObj_8)
  
  local dispObj_9 = display.newImageRect( crystaliteFileName, 22, 19 )
  dispObj_9.x = 169
  dispObj_9.y = 195
  --crystalContainer:insert(dispObj_9)
  
  local dispObj_10 = display.newImageRect( crystaliteFileName, 22, 19 )
  dispObj_10.x = 134
  dispObj_10.y = 192
  --crystalContainer:insert(dispObj_10)
  
  local dispObj_11 = display.newImageRect( crystaliteFileName, 22, 19 )
  dispObj_11.x = 205
  dispObj_11.y = 110
  --crystalContainer:insert(dispObj_11)
  
  local dispObj_12 = display.newImageRect( crystaliteFileName, 22, 19 )
  dispObj_12.x = 205
  dispObj_12.y = 78
  --crystalContainer:insert(dispObj_12)
  
  local dispObj_13 = display.newImageRect( crystaliteFileName, 22, 19 )
  dispObj_13.x = 242
  dispObj_13.y = 79
  --crystalContainer:insert(dispObj_13)
  
  local dispObj_14 = display.newImageRect( crystaliteFileName, 22, 19 )
  dispObj_14.x = 243
  dispObj_14.y = 109
  --crystalContainer:insert(dispObj_14)
  
  local dispObj_15 = display.newImageRect( crystaliteFileName, 22, 19 )
  dispObj_15.x = 208
  dispObj_15.y = 143
  --crystalContainer:insert(dispObj_15)
  
  local dispObj_16 = display.newImageRect( crystaliteFileName, 22, 19 )
  dispObj_16.x = 244
  dispObj_16.y = 170
  --crystalContainer:insert(dispObj_16)
  
  local dispObj_17 = display.newImageRect( crystaliteFileName, 22, 19 )
  dispObj_17.x = 343
  dispObj_17.y = 196
  --crystalContainer:insert(dispObj_17)
  
  local dispObj_18 = display.newImageRect( crystaliteFileName, 22, 19 )
  dispObj_18.x = 310
  dispObj_18.y = 230
  --crystalContainer:insert(dispObj_18)
  
  local dispObj_19 = display.newImageRect( crystaliteFileName, 22, 19 )
  dispObj_19.x = 346
  dispObj_19.y = 230
  --crystalContainer:insert(dispObj_19)
  
  local dispObj_20 = display.newImageRect( crystaliteFileName, 22, 19 )
  dispObj_20.x = 274
  dispObj_20.y = 230
  --crystalContainer:insert(dispObj_20)
  
  local dispObj_21 = display.newImageRect( crystaliteFileName, 22, 19 )
  dispObj_21.x = 239
  dispObj_21.y = 232
  --crystalContainer:insert(dispObj_21)
  
  local dispObj_22 = display.newImageRect( crystaliteFileName, 22, 19 )
  dispObj_22.x = 200
  dispObj_22.y = 231
  --crystalContainer:insert(dispObj_22)
  
  local dispObj_23 = display.newImageRect( crystaliteFileName, 22, 19 )
  dispObj_23.x = 163
  dispObj_23.y = 231
  --crystalContainer:insert(dispObj_23)
  
  local dispObj_24 = display.newImageRect( crystaliteFileName, 22, 19 )
  dispObj_24.x = 129
  dispObj_24.y = 231
  --crystalContainer:insert(dispObj_24)
  
  local dispObj_25 = display.newImageRect( crystaliteFileName, 22, 19 )
  dispObj_25.x = 127
  dispObj_25.y = 260
  --crystalContainer:insert(dispObj_25)
  
  local dispObj_26 = display.newImageRect( crystaliteFileName, 22, 19 )
  dispObj_26.x = 166
  dispObj_26.y = 262
  --crystalContainer:insert(dispObj_26)
  
  local dispObj_27 = display.newImageRect( crystaliteFileName, 22, 19 )
  dispObj_27.x = 204
  dispObj_27.y = 261
  --crystalContainer:insert(dispObj_27)
  
  local dispObj_28 = display.newImageRect( crystaliteFileName, 22, 19 )
  dispObj_28.x = 250
  dispObj_28.y = 262
  --crystalContainer:insert(dispObj_28)
  
  local dispObj_29 = display.newImageRect( crystaliteFileName, 22, 19 )
  dispObj_29.x = 281
  dispObj_29.y = 264
  --crystalContainer:insert(dispObj_29)
  
  local dispObj_30 = display.newImageRect( crystaliteFileName, 22, 19 )
  dispObj_30.x = 316
  dispObj_30.y = 264
  --crystalContainer:insert(dispObj_30)
  
  local dispObj_31 = display.newImageRect( crystaliteFileName, 22, 19 )
  dispObj_31.x = 345
  dispObj_31.y = 268
  --crystalContainer:insert(dispObj_31)
  
  local children = {dispObj_1, dispObj_2, dispObj_3, dispObj_4, dispObj_5, dispObj_6, dispObj_7, dispObj_8, dispObj_9, dispObj_10,
    dispObj_11, dispObj_12, dispObj_13, dispObj_14, dispObj_15, dispObj_16, dispObj_17, dispObj_18, dispObj_19, dispObj_20,
    dispObj_21, dispObj_22, dispObj_23, dispObj_24, dispObj_25, dispObj_26, dispObj_27, dispObj_28, dispObj_29, dispObj_30,
    dispObj_31}
  ]]--
  local children = {}
  
  function crystalContainer:addToPhysics(physics, text)
    children = converter.getObjects(text)
    for k,v in pairs(children) do 
      children[k].objectType = content.CRYSTALITE_TYPE
      children[k].wasTouched = false
      children[k].x = children[k].x + derivedX
      children[k].y = children[k].y + derivedY
      physics.addBody(children[k], "static", {isSensor = true}) 
    end
    
    --for i,v in ipairs(asd) do
    --    print(v)
    --end
  end
  
  function crystalContainer:collisionHandler(event)
    
  end
  
  function crystalContainer:dispose()
    self:removeSelf()
    self = nil
  end
  
  function crystalContainer:getChildren()
    return children
  end
  
  -- todo: it is wrong way, too much stress on proc
  function crystalContainer:onFrameChanged(playerXPosition, playerYPosition)
    for k,v in pairs(children) do 
      if children[k].wasTouched == true then 
        --if children[k].tween ~= nil then
        --  children[k].tween:pause()
        --  children[k].tween = nil
        --end
        if children[k].tween == nil then
          children[k].tween = tween.new(children[k], content.CRYSTALITE_EASE_TIME,
          {
            x = playerXPosition,
            y = playerYPosition,
            alpha = 0
          },
          {
            transitionEase = easing.inOutQuadratic,
            onComplete = function()
              children[k]:removeSelf()
              children[k] = nil
            end
          })
        end
      end
    end
  end
  
  return crystalContainer
end



