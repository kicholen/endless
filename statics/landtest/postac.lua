--
-- created with TexturePacker (http://www.codeandweb.com/texturepacker)
--
-- $TexturePacker:SmartUpdate:d4b84694c3906106c0b286947af4c00c:cdd3714967e378ee7cf65579fa1e3edd:75b2d695750510fff0f1132b4254d587$
--
-- local sheetInfo = require("mysheet")
-- local myImageSheet = graphics.newImageSheet( "mysheet.png", sheetInfo:getSheet() )
-- local sprite = display.newSprite( myImageSheet , {frames={sheetInfo:getFrameIndex("sprite")}} )
--

local SheetInfo = {}

SheetInfo.sheet =
{
    frames = {
    
        {
            -- postac1
            x=2,
            y=2,
            width=36,
            height=80,

            sourceX = 0,
            sourceY = 0,
            sourceWidth = 40,
            sourceHeight = 80
        },
        {
            -- postac2
            x=2,
            y=164,
            width=30,
            height=80,

            sourceX = 0,
            sourceY = 0,
            sourceWidth = 40,
            sourceHeight = 80
        },
        {
            -- postac3
            x=2,
            y=246,
            width=28,
            height=80,

            sourceX = 3,
            sourceY = 0,
            sourceWidth = 40,
            sourceHeight = 80
        },
        {
            -- postac4
            x=2,
            y=84,
            width=36,
            height=78,

            sourceX = 1,
            sourceY = 0,
            sourceWidth = 40,
            sourceHeight = 80
        },
        {
            -- postac5
            x=2,
            y=328,
            width=24,
            height=80,

            sourceX = 5,
            sourceY = 0,
            sourceWidth = 40,
            sourceHeight = 80
        },
    },
    
    sheetContentWidth = 40,
    sheetContentHeight = 410
}

SheetInfo.frameIndex =
{

    ["postac1"] = 1,
    ["postac2"] = 2,
    ["postac3"] = 3,
    ["postac4"] = 4,
    ["postac5"] = 5,
}

function SheetInfo:getSheet()
    return self.sheet;
end

function SheetInfo:getFrameIndex(name)
    return self.frameIndex[name];
end

return SheetInfo
