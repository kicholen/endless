--
-- created with TexturePacker (http://www.codeandweb.com/texturepacker)
--
-- $TexturePacker:SmartUpdate:66929d1eea5f6946128428386a5dd097:4d07687df967bb6d980d32b4d9a02cf6:1cdd3392e1588a883059bc723d5c1533$
--
-- local sheetInfo = require("mysheet")
-- local myImageSheet = graphics.newImageSheet( "mysheet.png", sheetInfo:getSheet() )
-- local sprite = display.newSprite( myImageSheet , {frames={sheetInfo:getFrameIndex("sprite")}} )
--

local SheetInfo = {}

SheetInfo.sheet =
{
    frames = {
    
        {
            -- ptak1
            x=42,
            y=2,
            width=18,
            height=12,

            sourceX = 3,
            sourceY = 5,
            sourceWidth = 30,
            sourceHeight = 20
        },
        {
            -- ptak2
            x=22,
            y=2,
            width=18,
            height=12,

            sourceX = 3,
            sourceY = 5,
            sourceWidth = 30,
            sourceHeight = 20
        },
        {
            -- ptak3
            x=2,
            y=2,
            width=18,
            height=12,

            sourceX = 3,
            sourceY = 5,
            sourceWidth = 30,
            sourceHeight = 20
        },
    },
    
    sheetContentWidth = 62,
    sheetContentHeight = 16
}

SheetInfo.frameIndex =
{

    ["ptak1"] = 1,
    ["ptak2"] = 2,
    ["ptak3"] = 3,
}

function SheetInfo:getSheet()
    return self.sheet;
end

function SheetInfo:getFrameIndex(name)
    return self.frameIndex[name];
end

return SheetInfo
