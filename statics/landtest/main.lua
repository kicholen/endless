-----------------------------------------------------------------------------------------
--
-- main.lua
--
-----------------------------------------------------------------------------------------

-- Your code here
local physics = require( "physics" )
physics.start()
physics.setGravity( 0, 9.8 )
physics.setDrawMode("hybrid")
score = 0

local tlo = display.newImage( "tlov2.png", display.contentCenterX, display.contentCenterY )

local waterfall = display.newImage( "waterfall.png", display.contentCenterX, display.contentCenterY )

local gory = display.newImage( "gory.png", display.contentCenterX, 200 )
local gory2 = display.newImage( "gory.png", -display.contentCenterX, 200 )

local goryv = display.newImage( "gory2.png", display.contentCenterX, 200 )
local goryv2 = display.newImage( "gory2.png", -display.contentCenterX, 200 )

local cloud1 = display.newImage( "cloud1.png", 300, 20 )
local cloud2 = display.newImage( "cloud2.png", 200, 10 )
local cloud3 = display.newImage( "cloud3.png", 100, 40 )

local cloud4 = display.newImage( "cloud1.png", 400, 60 )
local cloud5 = display.newImage( "cloud2.png", 500, 70 )
local cloud6 = display.newImage( "cloud3.png", 600, 36 )

local floor1 = display.newImage( "floor1.png", 0, 300 )
local floor2 = display.newImage( "floor1.png", 800, 300 )
--local floor3 = display.newImage( "floor1.png", 1600, 300 )
local obstacle1 = display.newImage( "obstacle1.png", 800, 245 )
local obstacle2 = display.newImage( "obstacle2.png", 1200, 238 )


physics.addBody( floor1, "static", { friction=0, bounce=0 } )
physics.addBody( floor2, "static", { friction=0, bounce=0 } )
--physics.addBody( floor3, "static", { friction=0.5, bounce=0.3 } )
--physics.addBody( obstacle1, "static", { friction=0.5, bounce=0.3 } )
--physics.addBody( obstacle2, "static", { friction=0.5, bounce=0.3 } )

local sheetInfo = require("ptak") -- lua file that Texture packer published 
-- The new sprite API
local umaSheet = graphics.newImageSheet( "ptak.png", sheetInfo:getSheet())
local spriteOptions = { name="ptak", frames = {1, 2, 3, 2 }, time=600 }
local spriteInstance = display.newSprite( umaSheet, spriteOptions )

local sheetInfo2 = require("postac") -- lua file that Texture packer published 
-- The new sprite API
local umaSheet2 = graphics.newImageSheet( "postac.png", sheetInfo2:getSheet())
local spriteOptions2 = { name="postac", frames = {1, 2, 3, 4, 5 }, time=600 } -- 
spriteInstance2 = display.newSprite( umaSheet2, spriteOptions2 )
spriteInstance2.myName = spriteInstance2
spriteInstance.x = 800
spriteInstance.y = 40

spriteInstance2.x = 40
spriteInstance2.y = 224

 local myText = display.newText( score, 400, 10, native.systemFontBold, 12 )
--spriteInstance2:applyForce( 500, 2000, 20, 20 )

--grp:insert( spriteIstance2 )
--spriteInstance2:play()

 physics.addBody( spriteInstance2, { density=5.0, friction=0, bounce=0 })
 

local instructionLabel = display.newText( "Prototype", display.contentCenterX, 10 + 20, native.systemFont, 18 )
instructionLabel:setFillColor( 235/255, 235/255, 1 )

local tPrevious = system.getTimer()

local function star( event )
    tlo:removeEventListener( "touch", star )
    physics.start()
    physics.setDrawMode("hybrid")
    score = 0
end



local function li( event )
    tlo:addEventListener( "touch", star )
end

local function onCollision( event )
  
  physics.pause()
  score = 0
  timer.performWithDelay( 1000, li )

end

sjump = 0

local function jump ( event )
  if spriteInstance2.y > 221 then
    spriteInstance2:applyForce( 0, -3500, spriteInstance2.x, spriteInstance2.y )
    sjump = 0
  elseif sjump == 0 and spriteInstance2.y < 190 then
    spriteInstance2:applyForce( 0, -2000, spriteInstance2.x, spriteInstance2.y )
    sjump = 1
  end
end


--obstacle1:addEventListener( "preCollision", onCollision )
--obstacle2:addEventListener( "preCollision", onCollision )


local function move(event)
	
	local tDelta = event.time - tPrevious
	tPrevious = event.time

	local xOffset = ( 0.2 * tDelta )
	
  score = score + 1
	myText.text = score


	
	gory.x = gory.x - xOffset*0.2
	gory2.x = gory2.x - xOffset*0.5
	
	goryv.x = goryv.x - xOffset*1.5
  goryv2.x = goryv2.x - xOffset*1.3
  
  cloud1.x = cloud1.x - xOffset*0.3
  cloud2.x = cloud2.x - xOffset*1.5
  cloud3.x = cloud3.x - xOffset*2.0
  cloud4.x = cloud4.x - xOffset*0.9
  cloud5.x = cloud5.x - xOffset*0.2
  cloud6.x = cloud6.x - xOffset*1.3
  
  floor1.x = floor1.x - xOffset*1.4
  floor2.x = floor2.x - xOffset*1.4
 -- floor3.x = floor3.x - xOffset*1.4
  
  obstacle1.x = obstacle1.x - xOffset*1.4
  obstacle2.x = obstacle2.x - xOffset*1.4
  
  spriteInstance.x = spriteInstance.x - xOffset
	
	if gory.x < - gory.width  then
		gory:translate ( 800*2 , 0)
	end
  if goryv.x < - goryv.width  then
		goryv:translate ( 600*2 , 0)
	end
  if gory2.x < - gory2.width  then
		gory2:translate ( 800*2 , 0)
	end
  if goryv2.x < - goryv2.width  then
		goryv2:translate ( 600*2 , 0)
	end
  
  if cloud1.x < - cloud1.width  then
		cloud1:translate ( 800 , 0)
	end
  if cloud2.x < - cloud2.width  then
		cloud2:translate ( 800 , 0)
	end
  if cloud3.x < - cloud3.width  then
		cloud3:translate ( 800 , 0)
	end
  if cloud4.x < - cloud4.width  then
		cloud4:translate ( 800 , 0)
	end	
  if cloud5.x < - cloud5.width  then
		cloud5:translate ( 800 , 0)
	end  
  if cloud6.x < - cloud6.width  then
		cloud6:translate ( 800 , 0)
  end
  if spriteInstance.x < - 100  then
		spriteInstance:translate ( 800 , 0)
    spriteInstance.y = math.random(200)
	end  
 
  if floor1.x < - 600  then
		floor1:translate ( 1600 , 0)
  end
  if floor2.x < - 600  then
		floor2:translate ( 1600 , 0) 
  end
 -- if floor3.x < - 600  then
	--	floor3:translate ( 1600 , 0) 
 -- end
  if obstacle1.x < - 100  then
		obstacle1:translate ( 800 + math.random(200) , 0) 
  end
  if obstacle2.x < - 100  then
		obstacle2:translate ( 800 + math.random(200) , 0)   
  end
end

spriteInstance:play()
spriteInstance2:play()
tlo:addEventListener( "touch", jump )

Runtime:addEventListener("enterFrame",move)