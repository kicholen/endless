
local SheetInfo = {}

SheetInfo.sheet =
{
	frames = {
		
		{ name=assualt_run_R_00, x = 42, y = 49, width = 21, height = 47, sourceX=47, sourceY=16, sourceWidth=124 , sourceHeight=64 },
		{ name=assualt_run_R_01, x = 27, y = 194, width = 25, height = 47, sourceX=44, sourceY=16, sourceWidth=124 , sourceHeight=64 },
		{ name=assualt_run_R_02, x = 0, y = 97, width = 40, height = 46, sourceX=38, sourceY=16, sourceWidth=124 , sourceHeight=64 },
		{ name=assualt_run_R_03, x = 41, y = 145, width = 21, height = 47, sourceX=47, sourceY=16, sourceWidth=124 , sourceHeight=64 },
		{ name=assualt_run_R_04, x = 0, y = 194, width = 25, height = 47, sourceX=44, sourceY=16, sourceWidth=124 , sourceHeight=64 },
		{ name=assualt_run_R_05, x = 0, y = 49, width = 40, height = 46, sourceX=38, sourceY=16, sourceWidth=124 , sourceHeight=64 },
    { name=assualt_jump_00, x = 0, y = 145, width = 39, height = 47, sourceX=34, sourceY=16, sourceWidth=124 , sourceHeight=64 },
		{ name=assualt_jump_00a, x = 0, y = 0, width = 41, height = 47, sourceX=32, sourceY=16, sourceWidth=124 , sourceHeight=64 },
	},
	sheetContentWidth = 128,
	sheetContentHeight = 256
}
SheetInfo.frameIndex =
{

    ["assualt_run_R_00"] = 1,
    ["assualt_run_R_01"] = 2,
    ["assualt_run_R_02"] = 3,
    ["assualt_run_R_03"] = 4,
    ["assualt_run_R_04"] = 5,
    ["assualt_run_R_05"] = 6,
    ["assualt_jump_00"] = 7,
    ["assualt_jump_00a"] = 8,
}

function SheetInfo:getSheet()
    return self.sheet;
end

function SheetInfo:getFrameIndex(name)
    return self.frameIndex[name];
end

return SheetInfo