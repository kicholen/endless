--
-- created with TexturePacker (http://www.codeandweb.com/texturepacker)
--
-- $TexturePacker:SmartUpdate:ef18f8f78bbf60922ac6c15828280939:289f688d20f680909d81d63482d4b68a:7029a1fca46f42441810c383c3f2eec3$
--
-- local sheetInfo = require("mysheet")
-- local myImageSheet = graphics.newImageSheet( "mysheet.png", sheetInfo:getSheet() )
-- local sprite = display.newSprite( myImageSheet , {frames={sheetInfo:getFrameIndex("sprite")}} )
--

local SheetInfo = {}

SheetInfo.sheet =
{
    frames = {
    
        {
            -- ammo
            x=1530,
            y=174,
            width=70,
            height=70,

        },
        {
            -- bg
            x=1126,
            y=174,
            width=320,
            height=480,

        },
        {
            -- bg2
            x=804,
            y=174,
            width=320,
            height=480,

        },
        {
            -- boom
            x=258,
            y=638,
            width=10,
            height=10,

        },
        {
            -- box
            x=2,
            y=604,
            width=50,
            height=50,

        },
        {
            -- bullet
            x=76,
            y=646,
            width=9,
            height=5,

        },
        {
            -- bullet2
            x=273,
            y=638,
            width=5,
            height=9,

        },
        {
            -- buttonBlueOver_100
            x=156,
            y=604,
            width=100,
            height=40,

        },
        {
            -- buttonBlue_100
            x=54,
            y=604,
            width=100,
            height=40,

        },
        {
            -- cos
            x=1530,
            y=246,
            width=50,
            height=190,

        },
        {
            -- energy
            x=270,
            y=638,
            width=1,
            height=10,

        },
        {
            -- gun
            x=258,
            y=626,
            width=15,
            height=10,

        },
        {
            -- jet
            x=54,
            y=646,
            width=20,
            height=8,

        },
        {
            -- ludz
            x=1582,
            y=246,
            width=20,
            height=80,

        },
        {
            -- ludz_rakietowy
            x=1448,
            y=174,
            width=80,
            height=80,

        },
        {
            -- tlob
            x=804,
            y=2,
            width=800,
            height=170,

        },
        {
            -- tlov1
            x=2,
            y=2,
            width=800,
            height=600,

        },
        {
            -- trawa
            x=258,
            y=604,
            width=100,
            height=20,

        },
    },
    
    sheetContentWidth = 1606,
    sheetContentHeight = 656
}

SheetInfo.frameIndex =
{

    ["ammo"] = 1,
    ["bg"] = 2,
    ["bg2"] = 3,
    ["boom"] = 4,
    ["box"] = 5,
    ["bullet"] = 6,
    ["bullet2"] = 7,
    ["buttonBlueOver_100"] = 8,
    ["buttonBlue_100"] = 9,
    ["cos"] = 10,
    ["energy"] = 11,
    ["gun"] = 12,
    ["jet"] = 13,
    ["ludz"] = 14,
    ["ludz_rakietowy"] = 15,
    ["tlob"] = 16,
    ["tlov1"] = 17,
    ["trawa"] = 18,
}

function SheetInfo:getSheet()
    return self.sheet;
end

function SheetInfo:getFrameIndex(name)
    return self.frameIndex[name];
end

return SheetInfo
