
local SheetInfo = {}

SheetInfo.sheet =
{
	frames = {
		
		{ name=slice01_01, x = 0, y = 32, width = 29, height = 29, sourceX=0, sourceY=0, sourceWidth=29 , sourceHeight=29 },
		{ name=slice02_02, x = 31, y = 32, width = 27, height = 30, sourceX=0, sourceY=0, sourceWidth=27 , sourceHeight=30 },
		{ name=slice03_03, x = 31, y = 0, width = 29, height = 30, sourceX=0, sourceY=0, sourceWidth=29 , sourceHeight=30 },
		{ name=slice04_04, x = 0, y = 0, width = 29, height = 30, sourceX=0, sourceY=0, sourceWidth=29 , sourceHeight=30 },
	},
	sheetContentWidth = 64,
	sheetContentHeight = 64
}
SheetInfo.frameIndex =
{

    ["slice01_01"] = 1,
    ["slice02_02"] = 2,
    ["slice03_03"] = 3,
    ["slice04_04"] = 4,
}

function SheetInfo:getSheet()
    return self.sheet;
end

function SheetInfo:getFrameIndex(name)
    return self.frameIndex[name];
end

return SheetInfo