--
-- created with TexturePacker (http://www.codeandweb.com/texturepacker)
--
-- $TexturePacker:SmartUpdate:abf8f4ac633ec224c97bf24e0457d6be:e25c2bf6544cfe1fdb26067b94aa4bb2:054b280e4cd85bdafcd91027e1aff888$
--
-- local sheetInfo = require("mysheet")
-- local myImageSheet = graphics.newImageSheet( "mysheet.png", sheetInfo:getSheet() )
-- local sprite = display.newSprite( myImageSheet , {frames={sheetInfo:getFrameIndex("sprite")}} )
--

local SheetInfo = {}

SheetInfo.sheet =
{
    frames = {
    
        {
            -- assualt_run_R_00
            x=82,
            y=2,
            width=22,
            height=48,

            sourceX = 46,
            sourceY = 15,
            sourceWidth = 124,
            sourceHeight = 64
        },
        {
            -- assualt_run_R_01
            x=30,
            y=2,
            width=26,
            height=48,

            sourceX = 43,
            sourceY = 15,
            sourceWidth = 124,
            sourceHeight = 64
        },
        {
            -- assualt_run_R_02
            x=148,
            y=2,
            width=40,
            height=46,

            sourceX = 38,
            sourceY = 16,
            sourceWidth = 124,
            sourceHeight = 64
        },
        {
            -- assualt_run_R_03
            x=58,
            y=2,
            width=22,
            height=48,

            sourceX = 46,
            sourceY = 15,
            sourceWidth = 124,
            sourceHeight = 64
        },
        {
            -- assualt_run_R_04
            x=2,
            y=2,
            width=26,
            height=48,

            sourceX = 43,
            sourceY = 15,
            sourceWidth = 124,
            sourceHeight = 64
        },
        {
            -- assualt_run_R_05
            x=106,
            y=2,
            width=40,
            height=46,

            sourceX = 38,
            sourceY = 16,
            sourceWidth = 124,
            sourceHeight = 64
        },
    },
    
    sheetContentWidth = 190,
    sheetContentHeight = 52
}

SheetInfo.frameIndex =
{

    ["assualt_run_R_00"] = 1,
    ["assualt_run_R_01"] = 2,
    ["assualt_run_R_02"] = 3,
    ["assualt_run_R_03"] = 4,
    ["assualt_run_R_04"] = 5,
    ["assualt_run_R_05"] = 6,
}

function SheetInfo:getSheet()
    return self.sheet;
end

function SheetInfo:getFrameIndex(name)
    return self.frameIndex[name];
end

return SheetInfo
