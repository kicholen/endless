module(..., package.seeall)

windowManager = require ("src.managers.WindowManager")
spritesManager = require("src.managers.SpritesManager")

-- dev purposes
keyboardManager = require("src.managers.KeyboardManager")
objectManager = require("src.managers.ObjectManager")


--ads
playhaven = require ("plugin.playhaven")
local playhavenListener = function(event)
end

local init_options = {
    token = "d763241cf96944ad934589c6f94c9d7b",
    secret = "e3b5c234cfb04f4bb961e65fe4694480",
}

playhaven.init(playhavenListener, init_options)

-- global init

storyboard = require "storyboard"
centerX = display.contentWidth / 2
centerY = display.contentHeight / 2
stageWidth = display.contentWidth - display.screenOriginX
stageHeight = display.contentHeight - display.screenOriginY
top = display.screenOriginY
right = display.contentWidth - display.screenOriginX
bottom = display.contentHeight - display.screenOriginY
left = display.screenOriginX
-- this parameters are only for background
backgroundWidth = display.contentWidth + 2 * math.abs(display.screenOriginX)
backgroundHeight = display.contentHeight + 2 * math.abs(display.screenOriginY)


--graphic sheet init
sheetInfo = require("statics.spriteSheets.testGraphicSheet") -- lua file that Texture packer published
myImageSheet = graphics.newImageSheet( "statics/graphics/testGraphicSheet.png", sheetInfo:getSheet())

-- Define reference points locations anchor ponts
_topRef = 0
_bottomRef = 1
_leftRef = 0
_rightRef = 1
_centerRef = 0.5

local startDebugTextPosition = top
local allTexts = {}
local debugTextHeight = 16

function addDebugText(basicText, onFrameChangingText)
    startDebugTextPosition = startDebugTextPosition + debugTextHeight
    local asd = display.newText(basicText, left, startDebugTextPosition, nil, debugTextHeight)
    asd.basicText = basicText
    asd.onFrameChangedFunction = function()
        asd.text = asd.basicText .. onFrameChangingText()
    end
    allTexts[#allTexts + 1] = asd
end
function removeDebugTexts()	for k,v in pairs (allTexts) do		allTexts [k]:removeSelf()
	    allTexts [k] = nil
	end	startDebugTextPosition = top + debugTextHeightend
-- fps
local fps
local previousTime = system.getTimer()

local function addFpsCounter()
    fps = display.newText("Fps: ", left, top, nil, debugTextHeight)
    fps.basicText = "Fps: "
    fps:setFillColor(1.0)
    fps.previousTime = previousTime
end

local function initMainDisplayOptions()
    -- hide the status bar
    display.setStatusBar( display.HiddenStatusBar )
    -- default to TopLeft anchor point for new objects
    display.setDefault( "anchorX", 0.0 )	
    display.setDefault( "anchorY", 0.0 )
    addFpsCounter()
end


local function enterFrame(event)
    local deltaTime = event.time - previousTime
    previousTime = event.time
    if event.time - fps.previousTime > 500 then
        fps.previousTime = event.time
        fps.text = fps.basicText .. string.format('%.2f', 1000 / deltaTime)
    end
    
    for i = 1, #allTexts do
        allTexts[i].onFrameChangedFunction()
    end
end

function init()
    initMainDisplayOptions()
end

Runtime:addEventListener("enterFrame", enterFrame)
