-----------------------------------------------------------------------------------------
--
-- main.lua
--
-----------------------------------------------------------------------------------------
require("mobdebug").start()
_context = require "Context"

-- init mainDisplay stuff here
local function initContext()
    _context.init()
end

initContext()
_context.windowManager.changeScene("src.scenes.menuScene")